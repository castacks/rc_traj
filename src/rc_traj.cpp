#include "ros/ros.h"
#include "tf_utils/tf_utils.h"
#include "std_msgs/String.h"
#include "rc_status_msgs/RCChannels.h"
#include "ca_nav_msgs/PathXYZVPsi.h"
#include <Eigen/Geometry>

nav_msgs::Odometry lookahead_odo;
bool rc_received = false;
bool odo_recvd = false;
ca_nav_msgs::PathXYZVPsi last_path_;


double roll,pitch,yaw,throttle;

double AngleDiff(double a,double b){
    double dif = fmod(b - a + M_PI,2*M_PI);
    if (dif < 0)
        dif += 2*M_PI;
    return dif - M_PI;
}

double ConstrainAngle(double x){
    x = fmod(x + M_PI,2*M_PI);
    if (x < 0)
        x += 2*M_PI;
    return x - M_PI;
}

int FindNextWp(Eigen::Vector3d &curr_pos, ca_nav_msgs::PathXYZVPsi &path){
    if(path.waypoints.size()<1)
        return -1;
    if(path.waypoints.size()<2){
        ROS_ERROR_STREAM("This should never happen.");
        return 0; // This never happens
    }
    int index = 0;
    double eps = 0.001;
    for(index=0; index<(path.waypoints.size()-1);index++){
        ca_nav_msgs::XYZVPsi &i = path.waypoints[index];
        Eigen::Vector3d wp_pos0(i.position.x, i.position.y, i.position.z);
        i = path.waypoints[index+1];
        Eigen::Vector3d wp_pos1(i.position.x, i.position.y, i.position.z);
        Eigen::Vector3d v1 = wp_pos0 - curr_pos;
        double dist1 = v1.norm();
        Eigen::Vector3d v2 = wp_pos1 - curr_pos;
        double dist2 = v2.norm();
        //Eigen::Vector3d path_vec = v2-v1; path_vec.normalize();
        if(dist1 < eps){
            return (index+1);
        }
        if(dist2 < eps){
            if((index+2) < (path.waypoints.size()-1)){
                return (index+2);
            }
            else{
                return -1;
            }

        }
        v1.normalize(); v2.normalize();
        double dot_prod = v1.dot(v2);
        if((dot_prod +1.) < eps){
            return (index+1);
        }
        if(dist1 < dist2){
            return index;
        }
        index++;
    }
    return -1;
}

ca_nav_msgs::PathXYZVPsi MakeRCTrajectory(Eigen::Vector3d current_pos, double current_heading, double acceleration,
                                                  double roll, double pitch, double yaw, double throttle, double time, double max_velocity, double max_yaw_rate,std::string frame, Eigen::Vector3d &vel){
    // Assuming roll, pitch, yaw vary from -0.5,0.5
    ca_nav_msgs::PathXYZVPsi path;
    path.header.stamp = ros::Time::now();
    path.header.frame_id = frame;
    Eigen::Vector3d v((pitch/0.5),(roll/0.5),0.); //
    double norm = v.norm();
    //ROS_ERROR_STREAM("v::"<<v.x()<<" "<<v.y()<<" "<<v.z());
    if(norm == 0 && yaw ==0.){
        return path;
    }
    //Eigen::Vector3d v_max(0.5,0.5,0.5);
    //double max_norm = v_max.norm();
    if(norm != 0)
        v = (v/v.norm())*max_velocity;
    v.z() = -3.*(throttle/0.5);
    //ROS_ERROR_STREAM("v::"<<v.x()<<" "<<v.y()<<" "<<v.z());
    Eigen::AngleAxisd rot(current_heading,Eigen::Vector3d(0.,0.,1.));
    v = rot*v;
    vel = v;
    yaw = (yaw/0.5)*max_yaw_rate;
    //ROS_ERROR_STREAM("pry::"<<pitch<<" "<<roll<<" "<<yaw);
    //ROS_ERROR_STREAM("v::"<<v.x()<<" "<<v.y()<<" "<<v.z());
    //ROS_ERROR_STREAM("rpy::"<<roll<<" "<<pitch<<" "<<yaw);

    ca_nav_msgs::XYZVPsi wp;
// First waypoint
    int index = -1;
    if(last_path_.waypoints.size()>0){
        index = FindNextWp(current_pos, last_path_);
        if(index>(last_path_.waypoints.size()-2))
            index = -1;
    }
    double vv;
    //ROS_ERROR_STREAM("Acc::"<<acceleration);
    if(index > 0){
        path.waypoints.push_back(last_path_.waypoints[index]);
        current_pos.x() = last_path_.waypoints[index+1].position.x;
        current_pos.y() = last_path_.waypoints[index+1].position.y;
        current_pos.z() = last_path_.waypoints[index+1].position.z;
        vv = last_path_.waypoints[index+1].vel;
        Eigen::Vector3d prev_pos;
        prev_pos.x() = last_path_.waypoints[index].position.x;
        prev_pos.y() = last_path_.waypoints[index].position.y;
        prev_pos.z() = last_path_.waypoints[index].position.z;
        Eigen::Vector3d dir = current_pos - prev_pos; dir.normalize();
        v = dir*vv;
        ROS_ERROR_STREAM("Found a previous index v::"<<v.x()<<"::"<<v.y()<<"::"<<v.z());
    }
    //target vel is vel
    // v is the current v
    double eps = 0.001;
    for(double delt=0.; delt<10.0; delt = delt+1.0){
        wp.position.x = current_pos.x() + v.x()*delt;
        wp.position.y = current_pos.y() + v.y()*delt;
        wp.position.z = current_pos.z() + v.z()*delt;
        wp.vel = v.norm();
        wp.heading = current_heading + yaw*delt;
        ROS_ERROR_STREAM("#"<<delt<<" WP pos::"<<wp.position.x<<"::"<<wp.position.y<<"::"<<wp.position.z);
        ROS_ERROR_STREAM("#"<<delt<<" WP vel::"<<wp.vel);
        path.waypoints.push_back(wp);
        Eigen::Vector3d correction = vel-v;
        if(correction.norm() > eps)
            correction.normalize();

        v = v + acceleration*correction*delt;
        ROS_ERROR_STREAM("#"<<delt<<" WP Next vel::"<<v.x()<<"::"<<v.y()<<"::"<<v.z());
        ROS_ERROR_STREAM("#"<<delt<<" Correction::"<<correction.x()<<"::"<<correction.y()<<"::"<<correction.z());

    }
    last_path_ = path;
    return path;
}



void RCCallback(const rc_status_msgs::RCChannels::ConstPtr& msg){
    rc_received = true;
    pitch = msg->pitch;
    pitch = std::max(pitch,-10000.);
    pitch = std::min(pitch, 10000.);
    pitch = pitch/20000.;

    yaw = msg->yaw;
    yaw = std::max(yaw,-10000.);
    yaw = std::min(yaw, 10000.);
    yaw = yaw/20000.;

    roll = msg->roll;
    roll = std::max(roll,-10000.);
    roll = std::min(roll, 10000.);
    roll = roll/20000.;

    throttle = msg->throttle;
    throttle = std::max(throttle,-10000.);
    throttle = std::min(throttle, 10000.);
    throttle = throttle/20000.;

}

void OdometryCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
  lookahead_odo = *msg;
  odo_recvd = true;
}



int main(int argc, char **argv)
{

  ros::init(argc, argv, "rc_trajectory");
  ros::NodeHandle n("~");


  std::string radio_topic;
  if (!n.getParam("radio_topic", radio_topic)){
    ROS_ERROR_STREAM("Could not get radio_topic parameter.");
    return -1;
  }

  std::string path_topic;
  if (!n.getParam("path_topic", path_topic)){
    ROS_ERROR_STREAM("Could not get path_topic parameter.");
    return -1;
  }

  std::string pose_topic;
  if (!n.getParam("pose_topic", pose_topic)){
    ROS_ERROR_STREAM("Could not get pose_topic parameter.");
    return -1;
  }


  ros::Publisher path_pub = n.advertise<ca_nav_msgs::PathXYZVPsi>( path_topic, 1 );

  ros::Subscriber lookahead_sub = n.subscribe(pose_topic, 1, OdometryCallback);
  ros::Subscriber rc_sub = n.subscribe(radio_topic,1,RCCallback);


  std::string trajectory_frame = "/world";
  if (!n.getParam("trajectory_frame", trajectory_frame)){
    ROS_ERROR_STREAM("Could not get trajectory frame parameter.");
    return -1;
  }

  std::string param;
  Eigen::Vector3d current_pos;
  double max_speed; double max_yaw_rate; double traj_time;

  param = "max_velocity";
  if (!n.getParam(param, max_speed)){
    ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
    exit(-1);
  }

  param = "max_yaw_rate";
  if (!n.getParam(param, max_yaw_rate)){
    ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
    exit(-1);
  }

  param = "traj_time";
  if (!n.getParam(param, traj_time)){
    ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
    exit(-1);
  }

  double acceleration;
  param = "acceleration";
  if (!n.getParam(param, acceleration)){
    ROS_ERROR_STREAM("Could not get "<< param<< " parameter.");
    exit(-1);
  }


  ros::Rate r(10);
  int count=0;
  while(ros::ok()){
      if(odo_recvd && count%5==0){
          nav_msgs::Odometry odom = lookahead_odo;
          current_pos.x()= odom.pose.pose.position.x;
          current_pos.y()= odom.pose.pose.position.y;
          current_pos.z()= odom.pose.pose.position.z;
          tf::Pose pose;
          tf::poseMsgToTF(odom.pose.pose, pose);
          double current_heading = tf::getYaw(pose.getRotation());
          Eigen::Vector3d op_vel;
          ca_nav_msgs::PathXYZVPsi path = MakeRCTrajectory(current_pos, current_heading,acceleration,
                                                                   roll, pitch, yaw, throttle,traj_time,max_speed,max_yaw_rate, trajectory_frame,op_vel);
          if(path.waypoints.size()>0){
            ca_nav_msgs::XYZVPsi wp = path.waypoints[0];
            Eigen::Vector3d pos1 = Eigen::Vector3d(wp.position.x,wp.position.y, wp.position.z);
            //ROS_ERROR_STREAM("PATH ERROR::"<<(pos1-current_pos).norm());
          }
          //ROS_WARN("Op velx:: %0.2f vely:: %0.2f velz:: %0.2f",op_vel.x(),op_vel.y(),op_vel.z());
          //ROS_WARN_STREAM("Scale::"<<op_vel.x()/ip_vx<<" vely::"<<op_vel.y()/ip_vy<<" velz::"<<op_vel.z()/ip_vz);
          path_pub.publish(path);

      }
      if(!odo_recvd) ROS_WARN_STREAM("rc_traj::Waiting for odometry.");
      if(!rc_received) ROS_WARN_STREAM("rc_traj::Waiting for RC.");
      r.sleep();
      count++;
      ros::spinOnce();
  }
  return 0;
}
